export interface Base {
  toJSON(): Record<string, any>;
}
