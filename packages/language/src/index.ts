import * as AST from './ast'; // Import and export instead of just export because of the TS bug.
import { Parser } from './parser';

export function parse(source: string) {
  return new Parser(source).parseDocument();
}

export { AST };
