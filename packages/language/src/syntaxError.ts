import { ASTNode } from './ast';
import { Source, SourceLocation } from './source';
import { getLocation } from './lexer/location';
import { printLocation, printSourceLocation } from './lexer/printLocation';

export class SyntaxError extends Error {
  message: string;
  locations: ReadonlyArray<SourceLocation> | void;
  path: ReadonlyArray<string | number> | void;
  nodes: ReadonlyArray<ASTNode> | void;
  source: Source | void;
  positions: ReadonlyArray<number> | void;
  originalError?: Error;

  constructor(
    message: string,
    nodes?: ReadonlyArray<ASTNode> | ASTNode | void | null,
    source?: Source,
    positions?: ReadonlyArray<number>,
    path?: ReadonlyArray<string | number>,
    originalError?: Error,
  ) {
    super(message);

    const _nodes = Array.isArray(nodes)
      ? nodes.length !== 0
        ? nodes
        : undefined
      : nodes
      ? [nodes]
      : undefined;

    // Compute locations in the source for the given nodes/positions.
    let _source = source;
    if (!_source && _nodes) {
      _source = _nodes[0].loc?.source;
    }

    let _positions = positions;
    if (!_positions && _nodes) {
      _positions = _nodes.reduce((list, node) => {
        if (node.loc) {
          list.push(node.loc.start);
        }
        return list;
      }, []);
    }
    if (_positions && _positions.length === 0) {
      _positions = undefined;
    }

    let _locations;
    if (positions && source) {
      _locations = positions.map((pos) => getLocation(source, pos));
    } else if (_nodes) {
      _locations = _nodes.reduce((list, node) => {
        if (node.loc) {
          list.push(getLocation(node.loc.source, node.loc.start));
        }
        return list;
      }, []);
    }

    this.source = _source;
    this.positions = _positions;
    this.locations = _locations;
    this.nodes = _nodes;
  }

  toJSON() {
    return {
      message: this.message,
      locations: this.locations,
    };
  }

  toString(): string {
    return printError(this);
  }
}

export function printError(error: SyntaxError): string {
  let output = error.message;

  if (error.nodes) {
    for (const node of error.nodes) {
      if (node.loc) {
        output += '\n\n' + printLocation(node.loc);
      }
    }
  } else if (error.source && error.locations) {
    for (const location of error.locations) {
      output += '\n\n' + printSourceLocation(error.source, location);
    }
  }

  return output;
}

export function syntaxError(
  source: Source,
  position: number,
  description: string,
) {
  return new SyntaxError(`Syntax Error: ${description}`, undefined, source, [
    position,
  ]);
}
