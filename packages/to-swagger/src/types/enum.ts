import { Base } from './base';
import { StringLiteral } from './stringLiteral';

export class Enum implements Base {
  constructor(
    public members: {
      name: string;
      value: string;
    }[],
  ) {}

  property(name: string) {
    const member = this.members.find((m) => m.name === name);
    return new StringLiteral(member.value ?? member.name);
  }

  toJSON() {
    return {
      type: 'string',
      enum: this.members.map((m) => m?.value || m.name),
    };
  }
}
