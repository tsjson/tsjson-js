import { Base } from './base';

export class Union implements Base {
  constructor(public types: Base[]) {}

  toJSON() {
    return {
      oneOf: this.types.map((t) => t.toJSON()),
    };
  }
}
