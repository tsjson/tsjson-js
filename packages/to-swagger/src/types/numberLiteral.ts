import { Base } from './base';

export class NumberLiteral implements Base {
  constructor(public value: number) {}

  toJSON() {
    return {
      type: 'number',
      enum: [this.value],
    };
  }
}
