import { Location } from '../lexer/location';
import { Kind } from './kind';

export function isNode(maybeNode: any): boolean {
  return maybeNode != null && typeof maybeNode.kind === 'string';
}

export type ASTNode =
  | DocumentNode
  | NameNode
  | DefinitionNode
  | TypeDefinitionNode
  | PropertySignatureNode
  | EnumMemberNode
  | TypeParameterNode;

export type NameNode = {
  kind: Kind.Name;
  loc?: Location;
  value: string;
};

export type DocumentNode = {
  kind: Kind.Document;
  loc?: Location;
  definitions: ReadonlyArray<any>;
};

export type DefinitionNode = TypeAliasNode | EnumNode;
export type TypeDefinitionNode =
  | TypeReferenceNode
  | TypeLiteralNode
  | ParenthesizedTypeNode
  | ArrayTypeNode
  | OperationNode
  | TupleNode
  | StringLiteralNode
  | IntNode
  | FloatNode
  | PropertyAccessNode
  | IndexAccessNode;

export type OperationNode = UnionTypeNode | IntersectionTypeNode;

export type TypeReferenceNode = {
  kind: Kind.TypeReference;
  name: NameNode;
  args?: TypeDefinitionNode[];
  loc?: Location;
};

export type TypeAliasNode = {
  kind: Kind.TypeAlias;
  name: NameNode;
  parameters: TypeParameterNode[];
  definition: TypeDefinitionNode;
  loc?: Location;
};

export type TypeParameterNode = {
  kind: Kind.TypeParameter;
  name: NameNode;
  loc?: Location;
};

export type TypeLiteralNode = {
  kind: Kind.TypeLiteral;
  properties?: PropertySignatureNode[];
  loc?: Location;
};

export type PropertySignatureNode = {
  kind: Kind.PropertySignature;
  name: NameNode;
  type: TypeDefinitionNode;
  optional?: boolean;
  loc?: Location;
};

export type UnionTypeNode = {
  kind: Kind.UnionType;
  types: TypeDefinitionNode[];
  loc?: Location;
};

export type IntersectionTypeNode = {
  kind: Kind.IntersectionType;
  types: TypeDefinitionNode[];
  loc?: Location;
};

export type ParenthesizedTypeNode = {
  kind: Kind.ParenthesizedType;
  type: TypeDefinitionNode;
  loc?: Location;
};

export type ArrayTypeNode = {
  kind: Kind.ArrayType;
  type: TypeDefinitionNode;
  loc?: Location;
};

export type TupleNode = {
  kind: Kind.Tuple;
  types: TypeDefinitionNode[];
  loc?: Location;
};

export type EnumNode = {
  kind: Kind.Enum;
  name: NameNode;
  members: EnumMemberNode[];
  loc?: Location;
};

export type EnumMemberNode = {
  kind: Kind.EnumMember;
  name: NameNode;
  value: StringLiteralNode;
  loc?: Location;
};

export type StringLiteralNode = {
  kind: Kind.StringLiteral;
  value: string;
  loc?: Location;
};

export type IntNode = {
  kind: Kind.Int;
  value: number;
  loc?: Location;
};

export type FloatNode = {
  kind: Kind.Float;
  value: number;
  loc?: Location;
};

export type IndexAccessNode = {
  kind: Kind.IndexAccess;
  target: TypeDefinitionNode;
  index: StringLiteralNode;
  loc?: Location;
};

export type PropertyAccessNode = {
  kind: Kind.PropertyAccess;
  target: TypeDefinitionNode;
  property: NameNode;
  loc?: Location;
};
