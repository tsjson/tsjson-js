import { parse, AST } from '@tsjson/language';
import { print } from './printer';

export const languages = [
  {
    name: 'tsjson',
    parsers: ['tsjson-parse'],
    extensions: ['.tsjson'],
    vscodeLanguageIds: ['tsjson'],
  },
];

export const parsers = {
  'tsjson-parse': {
    parse,
    astFormat: 'tsjson-ast',
    locStart: (node: AST.ASTNode) => {
      return node.loc.start;
    },
    locEnd: (node: AST.ASTNode) => {
      return node.loc.end;
    },
  },
};

export const printers = {
  'tsjson-ast': {
    print,
  },
};
