import { AST } from '@tsjson/language';
import { doc, FastPath, Doc } from 'prettier';

const {
  concat,
  join,
  indent,
  dedent,
  line,
  ifBreak,
  group,
  hardline,
  softline,
  breakParent,
} = doc.builders;

export function print(
  path: FastPath,
  options: object,
  print: (path: FastPath) => Doc,
): Doc {
  const node = path.getValue() as AST.ASTNode;

  switch (node.kind) {
    case AST.Kind.StringLiteral:
      return concat(['"', node.value, '"']);
    case AST.Kind.Document:
      return join(concat([line, line]), path.map(print, 'definitions'));
    case AST.Kind.TypeAlias:
      return join(' ', [
        'type',
        concat([
          node.name.value,
          node.parameters?.length > 0
            ? concat(['<', join(', ', path.map(print, 'parameters')), '>'])
            : '',
        ]),
        '=',
        group(path.call(print, 'definition')),
      ]);
    case AST.Kind.TypeReference:
      return concat([
        node.name.value,
        node.args.length > 0
          ? concat(['<', join(', ', path.map(print, 'args')), '>'])
          : '',
      ]);
    case AST.Kind.TypeLiteral:
      return group(
        concat([
          '{',
          node.properties.length > 0
            ? indent(concat([line, join(line, path.map(print, 'properties'))]))
            : '',
          line,
          '}',
        ]),
        { shouldBreak: node.properties.length > 0 },
      );
    case AST.Kind.PropertySignature:
      return group(
        concat([
          node.name.value,
          node.optional ? '?' : '',
          ': ',
          path.call(print, 'type'),
          ';',
        ]),
      );
    case AST.Kind.TypeParameter:
      return node.name.value;
    case AST.Kind.UnionType:
      return indent(
        concat([
          ifBreak(concat([line, '| ']), ''),
          join(concat([line, '| ']), path.map(print, 'types')),
        ]),
      );
    case AST.Kind.IntersectionType:
      return group(
        indent(join(concat([' &', line]), path.map(print, 'types'))),
      );
    case AST.Kind.ArrayType:
      return concat([path.call(print, 'type'), '[]']);
    case AST.Kind.ParenthesizedType:
      return group(
        concat([
          '(',
          path.call(print, 'type'),
          node?.type?.kind === AST.Kind.UnionType ? softline : '',
          ')',
        ]),
      );
    case AST.Kind.Tuple:
      return concat([
        '[',
        indent(softline),
        indent(join(concat([',', line]), path.map(print, 'types'))),
        softline,
        ']',
      ]);
    case AST.Kind.Enum:
      return group(
        concat([
          join(' ', ['enum', node.name.value, '{']),
          indent(
            concat([
              line,
              join(concat([',', line]), path.map(print, 'members')),
            ]),
          ),
          line,
          '}',
        ]),
        { shouldBreak: true },
      );
    case AST.Kind.EnumMember:
      if (node.value) {
        return concat([node.name.value, ' = ', path.call(print, 'value')]);
      }
      return concat([node.name.value]);
    case AST.Kind.Name:
      return node.value;
    case AST.Kind.Int:
    case AST.Kind.Float:
      return node.value.toString();
    case AST.Kind.PropertyAccess:
      return concat([
        path.call(print, 'target'),
        '.',
        path.call(print, 'property'),
      ]);
    case AST.Kind.IndexAccess:
      return concat([
        path.call(print, 'target'),
        '[',
        path.call(print, 'index'),
        ']',
      ]);
  }
}
