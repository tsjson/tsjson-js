import { Base } from './base';

export class StringLiteral implements Base {
  constructor(public value: string) {}

  toJSON() {
    return {
      type: 'string',
      enum: [this.value],
    };
  }
}
