import prettier from 'prettier';
import path from 'path';
import dedent from '../../../__testUtils__/dedent';

const format = (input: string) => {
  return prettier.format(input, {
    parser: 'tsjson-parse',
    plugins: [path.resolve(__dirname, '../src/index.ts')],
  });
};

test('Empty', () => {
  expect(format('')).toBe('');
});

test('Alias reference', () => {
  expect(format('type A=String')).toBe('type A = String');
});

test('Int literal', () => {
  expect(format('type A=1')).toBe('type A = 1');
});

test('Float literal', () => {
  expect(format('type A=1.1')).toBe('type A = 1.1');
});

test('String literal', () => {
  expect(format('type A="A"')).toBe('type A = "A"');
});

test('Two alias references', () => {
  expect(
    format(`
  type A=String
  type B=String
  `) + '\n',
  ).toBe(dedent`
  type A = String

  type B = String
  `);
});

test('Alias type literal', () => {
  expect(format('type A={field1:String; field2:Int;}')).toBe(dedent`
    type A = {
      field1: String;
      field2: Int;
    }`);
});

test('Type literal with operation', () => {
  expect(format('type A={field1: A | B;}')).toBe(dedent`
    type A = {
      field1: A | B;
    }`);
});

test('Type literal with no fields', () => {
  expect(format('type A={ }')).toBe(dedent`
    type A = { }`);
});

test('Type literal with operation multiline', () => {
  expect(
    format(
      'type A={field1: VeryLongAName|VeryLongBName|VeryLongCName|VeryLongDName|VeryLongEName;}',
    ),
  ).toBe(dedent`
    type A = {
      field1:
        | VeryLongAName
        | VeryLongBName
        | VeryLongCName
        | VeryLongDName
        | VeryLongEName;
    }`);
});

test('Optional property', () => {
  expect(format('type A={field?:String;}')).toBe(dedent`
    type A = {
      field?: String;
    }`);
});

test('Type parameters', () => {
  expect(format('type A<T1,T2> = T')).toBe(dedent`
    type A<T1, T2> = T`);
});

test('Type arguments', () => {
  expect(format('type A=G<T1,T2>')).toBe(dedent`
    type A = G<T1, T2>`);
});

test('Union', () => {
  expect(format('type T=A|B|C')).toBe(dedent`
    type T = A | B | C`);
});

test('Union multiline', () => {
  expect(
    format(
      'type VeryLongTypeName=VeryLongAName|VeryLongBName|VeryLongCName|VeryLongDName',
    ),
  ).toBe(dedent`
  type VeryLongTypeName =
    | VeryLongAName
    | VeryLongBName
    | VeryLongCName
    | VeryLongDName`);
});

test('Intersection', () => {
  expect(format('type T=A&B&C')).toBe(dedent`
    type T = A & B & C`);
});

test('Intersection multiline', () => {
  expect(
    format(
      'type VeryLongTypeName=VeryLongAName&VeryLongBName&VeryLongCName&VeryLongDName',
    ),
  ).toBe(dedent`
  type VeryLongTypeName = VeryLongAName &
    VeryLongBName &
    VeryLongCName &
    VeryLongDName`);
});

test('Array', () => {
  expect(format('type A=String[]')).toBe('type A = String[]');
});

test('Tuple', () => {
  expect(format('type A=[String, Int]')).toBe('type A = [String, Int]');
});

test('Tuple multiline', () => {
  expect(
    format(
      'type A=[String1, String2, String3, String4, String5, String6, String7, String8, String9]',
    ) + '\n',
  ).toBe(dedent`
    type A = [
      String1,
      String2,
      String3,
      String4,
      String5,
      String6,
      String7,
      String8,
      String9
    ]
  `);
});

test('Parenthesis', () => {
  expect(format('type A=(A | B)[]')).toBe('type A = (A | B)[]');
});

test('Parenthesis multiline', () => {
  expect(
    format(
      'type VeryLongTypeName=(VeryLongAName|VeryLongBName|VeryLongCName|VeryLongDName)[]',
    ) + '\n',
  ).toBe(dedent`
    type VeryLongTypeName = (
      | VeryLongAName
      | VeryLongBName
      | VeryLongCName
      | VeryLongDName
    )[]
    `);

  expect(
    format(
      'type VeryLongTypeName=(VeryLongAName&VeryLongBName&VeryLongCName&VeryLongDName)[]',
    ) + '\n',
  ).toBe(dedent`
      type VeryLongTypeName = (VeryLongAName &
        VeryLongBName &
        VeryLongCName &
        VeryLongDName)[]
      `);
});

test('Enum', () => {
  expect(
    format(`enum Type {
      V1,
      V2
    }`) + '\n',
  ).toBe(dedent`
  enum Type {
    V1,
    V2
  }
  `);
});

test('Enum with values', () => {
  expect(
    format(`enum Type {
      V1="v1",
      V2="v2"
    }`) + '\n',
  ).toBe(dedent`
  enum Type {
    V1 = "v1",
    V2 = "v2"
  }
  `);
});

test('Access index', () => {
  expect(
    format(
      `type A = any["index"]["index"].property.property["index"]["index"]`,
    ) + '\n',
  ).toBe(dedent`
    type A = any["index"]["index"].property.property["index"]["index"]
  `);
});
