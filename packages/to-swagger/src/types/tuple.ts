import { Base } from './base';

export class Tuple implements Base {
  constructor(public types: Base[]) {}

  toJSON() {
    return {
      type: 'array',
      items: this.types.map((t) => t.toJSON()),
    };
  }
}
