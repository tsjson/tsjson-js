/**
 * An exported enum describing the different kinds of tokens that the
 * lexer emits.
 */
export enum TokenKind {
  SOF = '<SOF>',
  EOF = '<EOF>',
  AMP = '&',
  PIPE = '|',
  PAREN_L = '(',
  PAREN_R = ')',
  BRACKET_L = '[',
  BRACKET_R = ']',
  BRACE_L = '{',
  BRACE_R = '}',
  CHEVRON_L = '<',
  CHEVRON_R = '>',
  COLON = ':',
  SEMICOLON = ';',
  EQUALS = '=',
  COMMA = ',',
  DOT = '.',
  QUESTION_MARK = '?',
  NAME = 'Name',
  INT = 'Int',
  FLOAT = 'Float',
  STRING = 'String',
  COMMENT = 'Comment',
}

export type OperationToken = TokenKind.AMP | TokenKind.PIPE;

const precedences = {
  [TokenKind.AMP]: 2, //
  [TokenKind.PIPE]: 1,
};
export function operationPrecedence(kind: OperationToken) {
  return precedences[kind];
}
