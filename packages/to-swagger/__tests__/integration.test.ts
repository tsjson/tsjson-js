import { SwaggerBuilder } from '../src';

const build = (source: string) => {
  const builder = new SwaggerBuilder();
  builder.parseSource(source);
  return builder.swagger();
};

test('Built in scalars', () => {
  expect(build(`type A = string`)?.Components?.Schemas).toEqual({
    A: { type: 'string' },
  });
  expect(build(`type A = int`)?.Components?.Schemas).toEqual({
    A: { type: 'integer' },
  });
  expect(build(`type A = boolean`)?.Components?.Schemas).toEqual({
    A: { type: 'boolean' },
  });
});

test('Object', () => {
  expect(
    build(`type A = {
    f1: string;
    f2?: int;
  }`)?.Components?.Schemas,
  ).toEqual({
    A: {
      type: 'object',
      properties: {
        f1: { type: 'string' },
        f2: { type: 'integer' },
      },
      required: ['f1'],
    },
  });
});

test('Array', () => {
  expect(build(`type A = string[]`)?.Components?.Schemas).toEqual({
    A: {
      type: 'array',
      items: {
        type: 'string',
      },
    },
  });
});

test('String literal', () => {
  expect(build(`type A = "a"`)?.Components?.Schemas).toEqual({
    A: {
      type: 'string',
      enum: ['a'],
    },
  });
});

test('Int literal', () => {
  expect(build(`type A = 1`)?.Components?.Schemas).toEqual({
    A: {
      type: 'number',
      enum: [1],
    },
  });
});

test('Float literal', () => {
  expect(build(`type A = 1.1`)?.Components?.Schemas).toEqual({
    A: {
      type: 'number',
      enum: [1.1],
    },
  });
});

test('Reference', () => {
  expect(
    build(`
    type A = 1.1;
    type B = A
  `)?.Components?.Schemas,
  ).toMatchObject({
    B: {
      $ref: '#/components/schemas/A',
    },
  });
});

test('Union', () => {
  expect(
    build(`
    type A = {f:"v1";}
    type B = {f:"v2";}
    type C = A | B
  `)?.Components?.Schemas,
  ).toMatchObject({
    C: {
      oneOf: [
        {
          $ref: '#/components/schemas/A',
        },
        {
          $ref: '#/components/schemas/B',
        },
      ],
    },
  });
});

test('Intersection', () => {
  expect(
    build(`
    type A = {f1:"v1";}
    type B = {f2:"v2";}
    type C = A & B
  `)?.Components?.Schemas,
  ).toMatchObject({
    C: {
      allOf: [
        {
          $ref: '#/components/schemas/A',
        },
        {
          $ref: '#/components/schemas/B',
        },
      ],
    },
  });
});

test('Tuple', () => {
  expect(
    build(`
    type A = [int, int]
  `)?.Components?.Schemas,
  ).toMatchObject({
    A: {
      type: 'array',
      items: [{ type: 'integer' }, { type: 'integer' }],
    },
  });
});

test('Parentheses', () => {
  expect(
    build(`
    type A = (string)
  `)?.Components?.Schemas,
  ).toMatchObject({
    A: {
      type: 'string',
    },
  });
});

test('Enum', () => {
  expect(
    build(`
    enum A {
      A, 
      B = "b"
    }
  `)?.Components?.Schemas,
  ).toMatchObject({
    A: {
      type: 'string',
      enum: ['A', 'b'],
    },
  });
});

test('Property reference', () => {
  expect(
    build(`
    enum A {
      A, 
      B = "b"
    }

    type T = A.B
  `)?.Components?.Schemas?.T,
  ).toMatchObject({
    type: 'string',
    enum: ['b'],
  });
});
