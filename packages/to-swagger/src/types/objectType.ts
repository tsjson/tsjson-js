import { Base } from './base';

export class ObjectType implements Base {
  constructor(
    public properties: {
      name: string;
      type: Base;
      optional: boolean;
    }[],
  ) {}

  toJSON() {
    const properties = {};
    const required = [];

    this.properties.forEach(({ name, type, optional }) => {
      properties[name] = type.toJSON();
      if (!optional) required.push(name);
    });

    return {
      type: 'object',
      properties: properties,
      required: required,
    };
  }
}
