import { TokenKind, OperationToken } from './tokenKind';
import { Token } from './token';

export function isPunctuatorTokenKind(kind: TokenKind): boolean {
  return (
    kind === TokenKind.PIPE ||
    kind === TokenKind.AMP ||
    kind === TokenKind.COLON ||
    kind === TokenKind.EQUALS ||
    kind === TokenKind.CHEVRON_L ||
    kind === TokenKind.CHEVRON_R ||
    kind === TokenKind.PAREN_L ||
    kind === TokenKind.PAREN_R ||
    kind === TokenKind.BRACKET_L ||
    kind === TokenKind.BRACKET_R ||
    kind === TokenKind.BRACE_L ||
    kind === TokenKind.BRACE_R
  );
}

export function isOperationToken(kind: TokenKind): kind is OperationToken {
  return (
    kind === TokenKind.PIPE || //
    kind === TokenKind.AMP
  );
}

export function charCode(str: string): number {
  return str.charCodeAt(0);
}

export function printCharCode(code: number): string {
  return (
    // NaN/undefined represents access beyond the end of the file.
    isNaN(code)
      ? TokenKind.EOF
      : // Trust JSON for ASCII.
      code < 0x007f
      ? JSON.stringify(String.fromCharCode(code))
      : // Otherwise print the escaped form.
        `"\\u${('00' + code.toString(16).toUpperCase()).slice(-4)}"`
  );
}

/**
 * A helper function to describe a token as a string for debugging.
 */
export function getTokenDesc(token: Token): string {
  const value = token.value;
  return getTokenKindDesc(token.kind) + (value != null ? ` "${value}"` : '');
}

/**
 * A helper function to describe a token kind as a string for debugging.
 */
export function getTokenKindDesc(kind: TokenKind): string {
  return isPunctuatorTokenKind(kind) ? `"${kind}"` : kind;
}
