import { Base } from './base';

export class Reference implements Base {
  constructor(public name: string, public type: Base) {}

  toJSON() {
    return {
      $ref: `#/components/schemas/${this.name}`,
    };
  }
}
