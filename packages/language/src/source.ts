export type SourceLocation = {
  line: number;
  column: number;
};

export class Source {
  body: string;
  name: string;
  locationOffset: SourceLocation;

  constructor(
    body: string,
    name: string = 'TSJSON Source',
    locationOffset: SourceLocation = { line: 1, column: 1 },
  ) {
    this.body = body;
    this.name = name;
    this.locationOffset = locationOffset;
  }
}

export function isSource(source: string | Source): source is Source {
  return typeof source !== 'string';
}
