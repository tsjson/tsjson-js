import { Base } from './base';

export class ArrayType implements Base {
  constructor(private ofType: Base) {}

  toJSON() {
    return {
      type: 'array',
      items: this.ofType.toJSON(),
    };
  }
}
