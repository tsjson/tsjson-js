import {
  Lexer,
  TokenKind,
  Token,
  getTokenDesc,
  getTokenKindDesc,
  Location,
  isOperationToken,
  operationPrecedence,
  OperationToken,
} from '../lexer';
import * as AST from '../ast';
import { Source, isSource } from '../source';
import { SyntaxError, syntaxError } from '../syntaxError';

export type ParserOptions = {};

export class Parser {
  private _options: ParserOptions;
  private _lexer: Lexer;

  constructor(source: string | Source, options?: ParserOptions) {
    const sourceObj = isSource(source) ? source : new Source(source);

    this._lexer = new Lexer(sourceObj);
    this._options = options;
  }

  parseDocument(): AST.DocumentNode {
    return {
      kind: AST.Kind.Document,
      definitions: this.parseOptionalMany(
        TokenKind.SOF,
        this.parseDefinition,
        TokenKind.EOF,
      ),
    };
  }

  parseDefinition(): AST.DefinitionNode {
    if (this.peek(TokenKind.NAME)) {
      switch (this.currentToken().value) {
        case 'type':
          return this.parseTypeAliasDefinition();
        case 'enum':
          return this.parseEnumDefinition();
      }
    }

    throw this.unexpected();
  }

  parseTypeAliasDefinition(): AST.TypeAliasNode {
    const start = this.expectToken(TokenKind.NAME);

    const name = this.parseName();
    const parameters = this.parseOptionalMany(
      TokenKind.CHEVRON_L,
      this.parseTypeParameter,
      TokenKind.CHEVRON_R,
      TokenKind.COMMA,
    );

    this.expectToken(TokenKind.EQUALS);
    const definition = this.parseOperation();

    this.expectOptionalToken(TokenKind.SEMICOLON);

    return {
      kind: AST.Kind.TypeAlias,
      name,
      parameters,
      definition,
      loc: this.loc(start),
    };
  }

  parseTypeParameter(): AST.TypeParameterNode {
    const start = this.currentToken();
    const name = this.parseName();
    const loc = this.loc(start);

    return {
      kind: AST.Kind.TypeParameter,
      name,
      loc,
    };
  }

  parseName(): AST.NameNode {
    const token = this.expectToken(TokenKind.NAME);
    return {
      kind: AST.Kind.Name,
      value: token.value,
      loc: this.loc(token),
    };
  }

  parseTypeDefinition(): AST.TypeDefinitionNode {
    let start = this.currentToken();
    let type: AST.TypeDefinitionNode;
    if (this.peek(TokenKind.NAME)) {
      type = this.parseTypeReference();
    } else if (this.peek(TokenKind.BRACE_L)) {
      type = this.parseTypeLiteral();
    } else if (this.peek(TokenKind.PAREN_L)) {
      type = this.parseParenthesizedType();
    } else if (this.peek(TokenKind.BRACKET_L)) {
      type = this.parseTuple();
    } else if (this.peek(TokenKind.STRING)) {
      type = this.parseStringLiteral();
    } else if (this.peek(TokenKind.INT)) {
      type = this.parseInt();
    } else if (this.peek(TokenKind.FLOAT)) {
      type = this.parseFloat();
    }

    if (!type) {
      throw this.unexpected();
    }

    while (
      [TokenKind.BRACKET_L, TokenKind.DOT].includes(this.currentToken().kind)
    ) {
      if (this.expectOptionalToken(TokenKind.BRACKET_L)) {
        if (this.expectOptionalToken(TokenKind.BRACKET_R)) {
          type = {
            kind: AST.Kind.ArrayType,
            type,
            loc: this.loc(start),
          };
        } else {
          const index = this.parseStringLiteral();
          this.expectToken(TokenKind.BRACKET_R);
          type = {
            kind: AST.Kind.IndexAccess,
            target: type,
            index,
            loc: this.loc(start),
          };
        }
      } else if (this.expectOptionalToken(TokenKind.DOT)) {
        const property = this.parseName();
        type = {
          kind: AST.Kind.PropertyAccess,
          target: type,
          property,
          loc: this.loc(start),
        };
      }
    }

    return type;
  }

  parseOperation(): AST.TypeDefinitionNode {
    this.expectOptionalToken(TokenKind.PIPE);
    const nodes: AST.TypeDefinitionNode[] = [this.parseTypeDefinition()];
    const operations: OperationToken[] = [];

    const executeOperation = () => {
      const operation = operations.shift();
      const right = nodes.pop();
      const left = nodes.pop();

      let newOperation: AST.OperationNode;
      if (operation === TokenKind.AMP) {
        if (left.kind === AST.Kind.IntersectionType) {
          newOperation = {
            kind: AST.Kind.IntersectionType,
            types: [...left.types, right],
          };
        } else {
          newOperation = {
            kind: AST.Kind.IntersectionType,
            types: [left, right],
          };
        }
      } else if (operation === TokenKind.PIPE) {
        if (left.kind === AST.Kind.UnionType) {
          newOperation = {
            kind: AST.Kind.UnionType,
            types: [...left.types, right],
          };
        } else {
          newOperation = {
            kind: AST.Kind.UnionType, //
            types: [left, right],
          };
        }
      }
      nodes.push(newOperation);
    };

    let operationKind;
    while (isOperationToken((operationKind = this.currentToken().kind))) {
      while (
        operations.length > 0 &&
        operationPrecedence(operations[0]) >= operationPrecedence(operationKind)
      ) {
        executeOperation();
      }

      operations.unshift(operationKind);
      this._lexer.advance();
      nodes.push(this.parseTypeDefinition());
    }

    while (operations.length > 0) {
      executeOperation();
    }

    return nodes[0];
  }

  parseStringLiteral(): AST.StringLiteralNode {
    const token = this.expectToken(TokenKind.STRING);
    return {
      kind: AST.Kind.StringLiteral,
      value: token.value,
      loc: this.loc(token),
    };
  }

  parseInt(): AST.IntNode {
    const token = this.expectToken(TokenKind.INT);
    return {
      kind: AST.Kind.Int,
      value: Number(token.value),
      loc: this.loc(token),
    };
  }

  parseFloat(): AST.FloatNode {
    const token = this.expectToken(TokenKind.FLOAT);
    return {
      kind: AST.Kind.Float,
      value: Number(token.value),
      loc: this.loc(token),
    };
  }

  parseTypeReference(): AST.TypeReferenceNode {
    const start = this.currentToken();
    const name = this.parseName();
    const args = this.parseOptionalMany(
      TokenKind.CHEVRON_L,
      this.parseOperation,
      TokenKind.CHEVRON_R,
      TokenKind.COMMA,
    );

    return {
      kind: AST.Kind.TypeReference,
      name,
      args,
      loc: this.loc(start),
    };
  }

  parseTuple(): AST.TupleNode {
    const start = this.currentToken();
    const types = this.parseMany(
      TokenKind.BRACKET_L,
      this.parseTypeDefinition,
      TokenKind.BRACKET_R,
      TokenKind.COMMA,
    );
    return {
      kind: AST.Kind.Tuple,
      types,
      loc: this.loc(start),
    };
  }

  parseTypeLiteral(): AST.TypeLiteralNode {
    const start = this.currentToken();
    const properties = this.parseOptionalMany(
      TokenKind.BRACE_L,
      this.parsePropertySignature,
      TokenKind.BRACE_R,
    );
    return {
      kind: AST.Kind.TypeLiteral,
      properties,
      loc: this.loc(start),
    };
  }

  parsePropertySignature(): AST.PropertySignatureNode {
    const start = this.currentToken();
    const name = this.parseName();
    let optional = false;
    if (this.expectOptionalToken(TokenKind.QUESTION_MARK)) {
      optional = true;
    }
    this.expectToken(TokenKind.COLON);
    const type = this.parseOperation();
    this.expectToken(TokenKind.SEMICOLON);

    return {
      kind: AST.Kind.PropertySignature,
      name,
      type,
      optional,
      loc: this.loc(start),
    };
  }

  parseParenthesizedType(): AST.ParenthesizedTypeNode {
    const start = this.expectToken(TokenKind.PAREN_L);
    const type = this.parseOperation();
    this.expectToken(TokenKind.PAREN_R);
    return {
      kind: AST.Kind.ParenthesizedType,
      type,
      loc: this.loc(start),
    };
  }

  parseEnumDefinition(): AST.EnumNode {
    const start = this.expectToken(TokenKind.NAME);
    const name = this.parseName();
    const members = this.parseMany(
      TokenKind.BRACE_L,
      this.parseEnumMember,
      TokenKind.BRACE_R,
      TokenKind.COMMA,
    );

    this.expectOptionalToken(TokenKind.SEMICOLON);

    return {
      kind: AST.Kind.Enum,
      name,
      members,
      loc: this.loc(start),
    };
  }

  parseEnumMember(): AST.EnumMemberNode {
    const start = this.currentToken();
    const name = this.parseName();

    let value: AST.StringLiteralNode;
    if (this.expectOptionalToken(TokenKind.EQUALS)) {
      value = this.parseStringLiteral();
    }

    return {
      kind: AST.Kind.EnumMember,
      name,
      value,
      loc: this.loc(start),
    };
  }

  /**
   * Returns current token from lexer
   */
  currentToken(): Token {
    return this._lexer.token;
  }

  /**
   * Helper function for creating an error when an unexpected lexed token is encountered.
   */
  unexpected(atToken?: Token): SyntaxError {
    const token = atToken ?? this.currentToken();
    return syntaxError(
      this._lexer.source,
      token.start,
      `Unexpected ${getTokenDesc(token)}.`,
    );
  }

  /**
   * Returns a location object, used to identify the place in the source that created a given parsed object.
   */
  loc(startToken: Token): Location {
    const endToken = this._lexer.lastToken;
    return new Location(startToken, endToken, this._lexer.source);
  }

  /**
   * Determines if the next token is of a given kind
   */
  peek(kind: TokenKind): boolean {
    return this.currentToken().kind === kind;
  }

  /**
   * Returns a non-empty list of parse nodes, determined by the parseFn.
   * This list begins with a lex token of openKind and ends with a lex token of closeKind.
   * Advances the parser to the next lex token after the closing token.
   */
  parseMany<T>(
    openKind: TokenKind,
    parseFn: () => T,
    closeKind: TokenKind,
    delimeterKind?: TokenKind,
  ): Array<T> {
    this.expectToken(openKind);
    const nodes = [];
    do {
      nodes.push(parseFn.call(this));
      if (delimeterKind && !this.peek(closeKind)) {
        this.expectToken(delimeterKind);
      }
    } while (!this.expectOptionalToken(closeKind));
    return nodes;
  }

  /**
   * Returns a list of parse nodes, determined by the parseFn.
   * It can be empty only if open token is missing otherwise it will always return non-empty list
   * that begins with a lex token of openKind and ends with a lex token of closeKind.
   * Advances the parser to the next lex token after the closing token.
   */
  parseOptionalMany<T>(
    openKind: TokenKind,
    parseFn: () => T,
    closeKind: TokenKind,
    delimeterKind?: TokenKind,
  ): Array<T> {
    if (this.expectOptionalToken(openKind)) {
      const nodes = [];
      while (!this.expectOptionalToken(closeKind)) {
        nodes.push(parseFn.call(this));
        if (delimeterKind && !this.peek(closeKind)) {
          this.expectToken(delimeterKind);
        }
      }
      return nodes;
    }
    return [];
  }

  /**
   * If the next token is of the given kind, return that token after advancing the lexer.
   * Otherwise, do not change the parser state and throw an error.
   */
  expectToken(kind: TokenKind): Token {
    const token = this.currentToken();
    if (token.kind === kind) {
      this._lexer.advance();
      return token;
    }

    throw syntaxError(
      this._lexer.source,
      token.start,
      `Expected ${getTokenKindDesc(kind)}, found ${getTokenDesc(token)}.`,
    );
  }

  /**
   * If the next token is of the given kind, return that token after advancing the lexer.
   * Otherwise, do not change the parser state and return undefined.
   */
  expectOptionalToken(kind: TokenKind): Token {
    const token = this.currentToken();
    if (token.kind === kind) {
      this._lexer.advance();
      return token;
    }
    return undefined;
  }
}
