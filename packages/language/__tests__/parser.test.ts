import { Kind } from '../src/ast';
import { TokenKind } from '../src/lexer';
import { Parser } from '../src/parser';

test('Document', () => {
  const p = new Parser(`
    type TestA = String
    type TestB = Int
  `);
  const ast = p.parseDocument();
  expect(ast.kind).toBe('Document');
  expect(ast.definitions.length).toBe(2);
});

describe('Definition', () => {
  const parseDefinition = (source: string) => {
    const p = new Parser(source);
    p.expectToken(TokenKind.SOF);
    const type = p.parseDefinition();
    p.expectToken(TokenKind.EOF);
    return type;
  };

  test('TypeAlias', () => {
    expect(parseDefinition(`type A = B`).kind).toBe('TypeAlias');
    expect(parseDefinition(`type A = B;`).kind).toBe('TypeAlias');
  });

  test('Enum', () => {
    expect(parseDefinition(`enum Enum { A, B, C }`).kind).toBe('Enum');
    expect(parseDefinition(`enum Enum { A, B, C };`).kind).toBe('Enum');
  });
});

describe('Type definition', () => {
  const parseTypeDefinition = (source: string) => {
    const p = new Parser(source);
    p.expectToken(TokenKind.SOF);
    const type = p.parseOperation();
    p.expectToken(TokenKind.EOF);
    return type;
  };

  test('TypeReference', () => {
    expect(parseTypeDefinition(`String`).kind).toBe('TypeReference');
  });

  test('TypeReference access property', () => {
    const ast = parseTypeDefinition(`Type.property`);

    expect(ast.kind).toBe('PropertyAccess');
    expect(ast.kind === Kind.PropertyAccess && ast.property.value).toBe(
      'property',
    );
    expect(ast.kind === Kind.PropertyAccess && ast.target.kind).toBe(
      'TypeReference',
    );
  });

  test('TypeReference access index', () => {
    const ast = parseTypeDefinition(`Type["property"]`);

    expect(ast.kind).toBe('IndexAccess');
    expect(ast.kind === Kind.IndexAccess && ast.index.value).toBe('property');
    expect(ast.kind === Kind.IndexAccess && ast.target.kind).toBe(
      'TypeReference',
    );
  });

  test('Chain of property and index accessors', () => {
    expect(
      parseTypeDefinition(`Type["property"]["property"].property.property`)
        .kind,
    ).toBe('PropertyAccess');

    expect(
      parseTypeDefinition(`Type.property.property["property"]["property"]`)
        .kind,
    ).toBe('IndexAccess');
  });

  test('StringLiteral', () => {
    expect(parseTypeDefinition(`"String"`).kind).toBe('StringLiteral');
  });

  test('Int', () => {
    const ast = parseTypeDefinition(`1`);
    expect(ast.kind).toBe('Int');
    expect(ast.kind === Kind.Int && ast.value).toBe(1);
  });

  test('Float', () => {
    const ast = parseTypeDefinition(`1.1`);
    expect(ast.kind).toBe('Float');
    expect(ast.kind === Kind.Float && ast.value).toBe(1.1);
  });

  test('Literal union', () => {
    expect(parseTypeDefinition(`"A" | "B"`).kind).toBe('UnionType');
    expect(parseTypeDefinition(`1 | 2`).kind).toBe('UnionType');
  });

  test('ArrayType', () => {
    expect(parseTypeDefinition(`String[]`).kind).toBe('ArrayType');
    expect(parseTypeDefinition(`(String|Int)[]`).kind).toBe('ArrayType');
  });

  test('Two-dimensional array', () => {
    const ast = parseTypeDefinition(`String[][]`);
    expect(ast.kind).toBe('ArrayType');
    expect(ast.kind === Kind.ArrayType && ast.type.kind).toBe('ArrayType');
  });

  test('TypeLiteral', () => {
    expect(parseTypeDefinition(`{ f: Type; }`).kind).toBe('TypeLiteral');
  });

  test('ParenthesizedType', () => {
    expect(parseTypeDefinition(`(A)`).kind).toBe('ParenthesizedType');
    expect(parseTypeDefinition(`(A & B)`).kind).toBe('ParenthesizedType');
  });

  test('UnionType', () => {
    expect(parseTypeDefinition(`A | B & C`).kind).toBe('UnionType');
    expect(parseTypeDefinition(`A & B | C`).kind).toBe('UnionType');
    expect(parseTypeDefinition(`A | (B & C)`).kind).toBe('UnionType');
    expect(
      parseTypeDefinition(`
    | A 
    | B
    `).kind,
    ).toBe('UnionType');
  });

  test('IntersectionType', () => {
    expect(parseTypeDefinition(`A & B`).kind).toBe('IntersectionType');
    expect(parseTypeDefinition(`A & (B | C)`).kind).toBe('IntersectionType');
  });

  test('Tuple', () => {
    expect(parseTypeDefinition(`[String, Number]`).kind).toBe('Tuple');
  });
});

describe('Type alias', () => {
  const parseTypeAlias = (source: string) => {
    const p = new Parser(source);
    p.expectToken(TokenKind.SOF);
    const type = p.parseTypeAliasDefinition();
    p.expectToken(TokenKind.EOF);
    return type;
  };

  test('Simple assignment', () => {
    const ast = parseTypeAlias(`type A = B`);
    expect(ast.kind).toBe('TypeAlias');
    expect(ast.name.value).toBe('A');
  });

  test('Generic with one parameter', () => {
    const ast = parseTypeAlias(`type A<T1> = B`);
    expect(ast.kind).toBe('TypeAlias');
    expect(ast.parameters[0].name.value).toBe('T1');
  });

  test('Generic with three parameters', () => {
    const ast = parseTypeAlias(`type A<T1, T2, T3> = B`);
    expect(ast.kind).toBe('TypeAlias');
    expect(ast.parameters[0].name.value).toBe('T1');
    expect(ast.parameters[2].name.value).toBe('T3');
  });
});

describe('TypeReference', () => {
  const parseTypeReference = (source: string) => {
    const p = new Parser(source);
    p.expectToken(TokenKind.SOF);
    const type = p.parseTypeReference();
    p.expectToken(TokenKind.EOF);
    return type;
  };

  test('Simple', () => {
    const ast = parseTypeReference(`A`);
    expect(ast.name.value).toBe('A');
  });

  test('Generic', () => {
    const ast = parseTypeReference(`A<T>`);
    expect(ast.name.value).toBe('A');
    expect(ast.args[0].kind).toBe('TypeReference');
    expect(ast.args[0].kind === 'TypeReference' && ast.args[0].name.value).toBe(
      'T',
    );
  });

  test('Generic with two arguments', () => {
    const ast = parseTypeReference(`A<T1, T2>`);
    expect(ast.name.value).toBe('A');
    expect(ast.args[0].kind).toBe('TypeReference');
    expect(ast.args[0].kind === 'TypeReference' && ast.args[0].name.value).toBe(
      'T1',
    );

    expect(ast.args[1].kind).toBe('TypeReference');
    expect(ast.args[1].kind === 'TypeReference' && ast.args[1].name.value).toBe(
      'T2',
    );
  });
});

describe('TypeLiteral', () => {
  const parseTypeLiteral = (source: string) => {
    const p = new Parser(source);
    p.expectToken(TokenKind.SOF);
    const type = p.parseTypeLiteral();
    p.expectToken(TokenKind.EOF);
    return type;
  };

  test('Simple', () => {
    const ast = parseTypeLiteral(`{ field: T; }`);
    expect(ast.properties[0].name.value).toBe('field');
    expect(ast.properties[0].type).toMatchObject({
      kind: Kind.TypeReference,
      name: { value: 'T' },
    });
  });

  test('No fields', () => {
    const ast = parseTypeLiteral(`{ }`);
    expect(ast.properties.length).toBe(0);
  });

  test('Optional', () => {
    const ast = parseTypeLiteral(`{ field?: T; }`);
    expect(ast.properties[0]).toMatchObject({
      optional: true,
    });
  });

  test('Two fields', () => {
    const ast = parseTypeLiteral(
      [`{`, `field1: T1;`, `field2: T2;`, `}`].join('\n'),
    );
    expect(ast.properties[0].name.value).toBe('field1');
    expect(ast.properties[1].name.value).toBe('field2');
  });

  test('With arguments', () => {
    const ast = parseTypeLiteral(`{ field?: T<T1, T2>; }`);
    expect(ast.properties[0].type.kind).toBe(Kind.TypeReference);
  });

  test('Operation', () => {
    const ast = parseTypeLiteral(`{ field: A | B; }`);
    expect(ast.properties[0].type.kind).toBe(Kind.UnionType);
  });
});

describe('Enum', () => {
  const parseEnumDefinition = (source: string) => {
    const p = new Parser(source);
    p.expectToken(TokenKind.SOF);
    const type = p.parseEnumDefinition();
    p.expectToken(TokenKind.EOF);
    return type;
  };

  test('Simple', () => {
    const ast = parseEnumDefinition(`enum E { A, B }`);
    expect(ast.members[0].name.value).toBe('A');
    expect(ast.members[1].name.value).toBe('B');
  });

  test('With values', () => {
    const ast = parseEnumDefinition(`enum E { 
      A = "a", 
      B = "b" 
    }`);

    expect(ast.members[0].name.value).toBe('A');
    expect(ast.members[0].value.value).toBe('a');
    expect(ast.members[1].name.value).toBe('B');
    expect(ast.members[1].value.value).toBe('b');
  });
});
