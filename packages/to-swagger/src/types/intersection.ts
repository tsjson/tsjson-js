import { Base } from './base';

export class Intersection implements Base {
  constructor(public types: Base[]) {}

  toJSON() {
    return {
      allOf: this.types.map((t) => t.toJSON()),
    };
  }
}
