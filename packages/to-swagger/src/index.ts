import { AST, parse } from '@tsjson/language';
import { split, last, map, toPairs, fromPairs, pipe } from 'ramda';

import {
  Base,
  TSJSONString,
  TSJSONInt,
  TSJSONFloat,
  TSJSONBoolean,
  ObjectType,
  ArrayType,
  StringLiteral,
  NumberLiteral,
  Reference,
  Union,
  Intersection,
  Tuple,
} from './types';
import { Enum } from './types/enum';

export class SwaggerBuilder {
  builtInTypes: Record<string, any>;
  types: { [name: string]: Base };

  constructor() {
    this.builtInTypes = {
      string: TSJSONString,
      int: TSJSONInt,
      float: TSJSONFloat,
      boolean: TSJSONBoolean,
    };
    this.types = {};
  }

  parseSource(source: string) {
    const ast = parse(source);
    ast.definitions.forEach((node) => this.typeDefinition(node));
  }

  addGeneric(
    name: string,
    parameters: AST.TypeParameterNode[],
    ast: AST.ASTNode,
  ) {
    throw 'need implementation';
  }

  typeDefinition(node: AST.ASTNode, scope = {}) {
    switch (node.kind) {
      case AST.Kind.TypeAlias: {
        const name = node.name.value;
        const parameters = node.parameters;
        const typeNode = node.definition;

        if (parameters.length > 0) {
          this.addGeneric(name, parameters, typeNode);
        } else {
          this.types[name] = this.typeDefinition(typeNode, scope);
        }
        return;
      }
      case AST.Kind.Enum: {
        const name = node.name.value;
        const members = node.members;
        this.types[name] = new Enum(
          members.map((m) => ({
            name: m.name.value,
            value: m?.value?.value,
          })),
        );
        return;
      }
      case AST.Kind.TypeReference:
        return this.reference(node, scope);
      case AST.Kind.TypeLiteral: {
        return new ObjectType(
          node.properties.map((prop) => ({
            name: prop.name.value,
            type: this.typeDefinition(prop.type, scope),
            optional: prop.optional,
          })),
        );
      }
      case AST.Kind.ArrayType:
        return new ArrayType(this.typeDefinition(node.type, scope));
      case AST.Kind.StringLiteral:
        return new StringLiteral(node.value);
      case AST.Kind.Int:
      case AST.Kind.Float:
        return new NumberLiteral(node.value);
      case AST.Kind.UnionType:
        return new Union(node.types.map((t) => this.typeDefinition(t, scope)));
      case AST.Kind.IntersectionType:
        return new Intersection(
          node.types.map((t) => this.typeDefinition(t, scope)),
        );
      case AST.Kind.Tuple:
        return new Tuple(node.types.map((t) => this.typeDefinition(t, scope)));
      case AST.Kind.ParenthesizedType:
        return this.typeDefinition(node.type, scope);
      case AST.Kind.PropertyAccess: {
        const target = this.typeDefinition(node.target);
        if (target instanceof Reference) {
          const type = target.type;
          if (type instanceof Enum) {
            return type.property(node.property.value);
          }
        }
        throw `Can't access property ${node.property.value} of type ${node.target}`;
      }
    }
    throw `Can't parse typeDefinition ${node.kind}`;
  }

  reference(node: AST.TypeReferenceNode, scope = {}) {
    const name = node.name.value;
    let type: any;
    if ((type = this.builtInTypes[name])) {
      return type;
    } else if ((type = this.types[name])) {
      return new Reference(name, type);
    }

    throw `Type doesn't exist: ${name}`;
  }

  swagger() {
    return {
      Components: {
        Schemas: fromPairs(
          map(([k, v]) => [k, v.toJSON()], toPairs(this.types)),
        ),
      },
    };
  }
}
