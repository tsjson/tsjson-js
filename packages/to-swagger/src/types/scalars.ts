import { Base } from './base';
class Scalar implements Base {
  constructor(public swaggerType: string) {}

  toJSON() {
    return { type: this.swaggerType };
  }
}

export const TSJSONString = new Scalar('string');
export const TSJSONInt = new Scalar('integer');
export const TSJSONFloat = new Scalar('float');
export const TSJSONBoolean = new Scalar('boolean');
