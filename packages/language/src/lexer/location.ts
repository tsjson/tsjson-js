import { Source, SourceLocation } from '../source';
import { Token } from './token';

/**
 * Contains a range of UTF-8 character offsets and token references that
 * identify the region of the source from which the AST derived.
 */
export class Location {
  /**
   * The character offset at which this Node begins.
   */
  start: number;

  /**
   * The character offset at which this Node ends.
   */
  end: number;

  /**
   * The Token at which this Node begins.
   */
  startToken: Token;

  /**
   * The Token at which this Node ends.
   */
  endToken: Token;

  /**
   * The Source document the AST represents.
   */
  source: Source;

  constructor(startToken: Token, endToken: Token, source: Source) {
    this.start = startToken.start;
    this.end = endToken.end;
    this.startToken = startToken;
    this.endToken = endToken;
    this.source = source;
  }

  toJSON(): { start: number; end: number } {
    return { start: this.start, end: this.end };
  }
}

export function getLocation(source: Source, position: number): SourceLocation {
  const lineRegexp = /\r\n|[\n\r]/g;
  let line = 1;
  let column = position + 1;
  let match;
  while ((match = lineRegexp.exec(source.body)) && match.index < position) {
    line += 1;
    column = position + 1 - (match.index + match[0].length);
  }
  return { line, column };
}
